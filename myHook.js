// helpers
const getLast = arr => arr[arr.length - 1]
const isFunction = arg => typeof arg === 'function'
const ifFunctionCallWith = (fn, args = []) =>
  isFunction(fn) ? fn(...args) : fn

// hooks
// set every function saved values
const map = new WeakMap()
const calledFnStack = []

function useState(initialState) {
  const statesRef = map.get(getLast(calledFnStack))
  const {nextIndex, states} = statesRef
  if (nextIndex === states.length) {
    states[nextIndex] = ifFunctionCallWith(initialState)
  }
  const setState = stateOrFnState =>
    (states[nextIndex] = ifFunctionCallWith(stateOrFnState, [
      states[nextIndex],
    ]))
  statesRef.nextIndex += 1
  return [states[nextIndex], setState]
}

function withHook(fn) {
  return function hookedFn(...args) {
    if (map.has(hookedFn)) {
      map.get(hookedFn).nextIndex = 0
    } else {
      map.set(hookedFn, {nextIndex: 0, states: []})
    }
    calledFnStack.push(hookedFn)
    const result = fn(...args)
    calledFnStack.pop()
    return result
  }
}

;(function test() {
  const App = withHook(function App() {
    const [count, setCount] = useState(0)
    return {
      count,
      setCount,
    }
  })

  let count, setCount
  ;({count, setCount} = App())
  console.log(count)
  setCount(3)
  ;({count, setCount} = App())
  console.log(count)
})()
